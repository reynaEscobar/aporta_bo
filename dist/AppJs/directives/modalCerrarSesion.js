angular.module('appRoot')
.directive('modalCerrarSesion', [
  'URLS_CONFIG', function(URLS_CONFIG){
  return {
    restrict: 'E',
    link: function($scope, element, attrs, controller) {
      $scope.cerrarSesion = function() {       

        if( amplify.store('inicio_sesion')) {
          if(amplify.store('inicio_sesion').tipoCuenta == 'google'){
            cerrarSesionGoogle();
          } else {
            cerrarSesionFacebook();
          }          
        } else {
          limpiarLocalStorage();
        }
      }

      var limpiarLocalStorage = function() {
        amplify.store('id_usuario', null, { expires: 1 });
        amplify.store('usuario', null, { expires: 1 });
        amplify.store('imagenPerfil', null);
        amplify.store('inicio_sesion', null);
        amplify.store('corridaSolicitud', null);
				window.location = 'Home.html';
      }
      
      var cerrarSesionGoogle = function() {
        gapi.load('auth2', function() { // Loads the auth2 component of gapi
					gapi.auth2.init({ // initialize the auth2 using your credentials
						client_id: URLS_CONFIG.clientIdGoogle
					}).then(function onInit() { // on complete of init
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
              limpiarLocalStorage();
            });
					});
				});
        
      }

      var cerrarSesionFacebook = function() {
        FB.getLoginStatus(function(response) {
          if (response && response.status === 'connected') {
           FB.logout(function(response) { 
            // document.location.reload();
            limpiarLocalStorage();            
           });
          } else {
            limpiarLocalStorage();
          }
         });
      }
    },
    templateUrl: 'modal-cerrar-sesion.html'
  }
}])