angular.module('appRoot')
	.controller('LoginController', [
		'$scope',
		'$location',
		'$routeParams',
		'ServiceRoutes',
		'localStorageService',

		function ($scope, $location, $routeParams, ServiceRoutes, localStorageService, $compile) {

			$scope._login = {};
			$scope.hour = 1000 * 60 * 60;

			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			var acute = "\u00E1",
			ecute = "\u00E9",
			icute = "\u00ED",
			ocute = "\u00F3",
			ucute = "\u00FA";

			$scope.init = function () {
				var lHash = location.hash;
				
				$scope._login.correo = ""
				$scope._login.password = ""

				$scope._correoRecuperacion = "";

				$scope._cambioContrasena = {
					contrasena: '',
					confirmacion: ''
				}
				$scope._login.mantener = false;
				
				$scope.obtenerToken();
			};
		  
			$scope.validateForm = function(){
				if (!$scope._login.correo) {
					Toast.fire({
						type: 'warning',
						title: 'El usuario es requerido.'
					})
					return false;
				}
				if (!$scope._login.password) {
					Toast.fire({
						type: 'warning',
						title: 'La contraseña es requerida.'
					})
					return false;
				}
				return true;
			}

		  $scope.goLogin = function () {
				if ($scope.validateForm()){
					ServiceRoutes.login({
						Correo: $scope._login.correo,
						Password: $scope._login.password,
						tokenDispositivo: "",
						tipoDispositivo: 3
					}).success(
						function (data) {
							if (data.IsValid) {
								// $scope.getCliente(data.Data);
								console.log('data', data)
								amplify.store('mantenerSesion', $scope._login.mantener);

								if ($scope._login.mantener){
									amplify.store('id_usuario', data.Data.id_usuario);
									amplify.store('usuario', data.Data);
								} else{
									amplify.store('id_usuario', data.Data.id_usuario, { expires: $scope.hour });
									amplify.store('usuario', data.Data, { expires: $scope.hour });
								}
								window.location = "dashboard.html";
							}
							else {
								Toast.fire({
									type: 'warning',
									title: data.Message
								})
							}
						}
					)
					.error
					(
					function (ex) {
						Toast.fire({
							type: 'warning',
							title: ex.Message
						  })
					})
				}
		  };

			$scope.getCliente = function (clienteData) {
				ServiceRoutes.getCliente({
					id: clienteData.id_usuario
				}).success(
					function (responseGetCliente) {
						if (responseGetCliente.IsValid == true) {
							ServiceRoutes.getSueldo(responseGetCliente.Data.id_centro_trabajo_empleado).success(
								function (responseGetSueldo) {
									if (responseGetSueldo.IsValid == true) {
										
										if (responseGetSueldo.Data.length != 0){
											responseGetCliente.ingreso_mensual = responseGetSueldo.Data[0].Sueldo;

											//si el sueldo no es igual actualizarlo con el de aporta
											if(responseGetSueldo.Data[0].Sueldo != responseGetCliente.ingreso_mensual) {
												this.editarCliente(clienteData, responseGetCliente);
											}
										}
										
										var data = { ...clienteData, ...responseGetCliente };
										amplify.store('mantenerSesion', $scope._login.mantener);

										if ($scope._login.mantener){
											amplify.store('id_usuario', data.id_usuario);
											amplify.store('usuario', data);
										} else{
											amplify.store('id_usuario', data.id_usuario, { expires: $scope.hour });
											amplify.store('usuario', data, { expires: $scope.hour });
										}

										window.location = 'dashboard.html';
									}
									else {
										Toast.fire({
											type: 'warning',
											title: responseGetSueldo.Message
										})
									}
								}
							)
							.error
							(
							function (ex) {
								Toast.fire({
									type: 'warning',
									title: ex.Message
									})
							})
						}
						else {
							Toast.fire({
								type: 'warning',
								title: responseGetCliente.Message
								})
						}
					}
				)
				.error
				(
				function (ex) {
					Toast.fire({
						type: 'warning',
						title: ex.Message
						})
				})
			};

			$scope.abrirModal = function() {
				$scope.mensajeError = '';
				$('#modal').modal('toggle');
			}

			$scope.validacionCambioContrasena = function(){
				if($scope._cambioContrasena.contrasena == "" || $scope._cambioContrasena.contrasena == undefined) {
					Toast.fire({
						type: 'warning',
						title: 'La nueva contraseña es requerida.'
					})
					return false;
				}
				if ($scope._cambioContrasena.contrasena != $scope._cambioContrasena.confirmacion ) {
					Toast.fire({
					  type: 'warning',
					  title: 'Las contraseñas no coinciden.'
					})
					return false;
				}

				return true;
			}
			
			$scope.recuperarContrasena = function() {
				$scope.mensajeError = '';
				if(!$scope._correoRecuperacion) {
					Toast.fire({
						type: 'warning',
						title: 'Debe ingresar un correo electrónico.'
					})

					return;
				}

				ServiceRoutes.recuperarContrasena({
					correo: $scope._correoRecuperacion
				}).success(
					function (response) {
						if (response.IsValid == true) {
							$('#modal').modal('toggle');
							$('#modalCorreoEnviado').modal('toggle');
						}
						else {
							$scope.mensajeError = response.Message;
						}
					}
				)
				.error
				(
				function (ex) {
					Toast.fire({
						type: 'warning',
						title: ex.Message
						})
				})
			}

			$scope.obtenerToken = function() {
				const params = new Map(location.search.slice(1).split('&').map(kv => kv.split('=')));
				$scope.token = params.get('token');
				var tokenIdUsuario = params.get('id');

				if($scope.token) {
					$('#modalCambiarPassword').modal('toggle');
				} else if(tokenIdUsuario) {
					if((tokenIdUsuario.length%4) > 0) {
						Toast.fire({
							type: 'warning',
							title: 'Token no valido.'
						})
						setTimeout(function(){
							window.location = "login.html";
						},500);
						return
					}
					$scope.activarUsuarioToken(tokenIdUsuario);
				}
			}

			$scope.cambiarContrasena = function() {
				if($scope.validacionCambioContrasena()) {
					ServiceRoutes.CambiarContrasenaConToken({
						contraseña_nueva: $scope._cambioContrasena.contrasena,
						token: $scope.token
					}).success(
						function (response) {
							if (response.IsValid == true) {
								$('#modalCambiarPassword').modal('toggle');
								Toast.fire({
									type: 'success',
									title: 'Contraseña actualizada correctamente.'
								})
							}
							else {
								Toast.fire({
									type: 'warning',
									title: response.Message
								})
							}
							setTimeout(function(){
								window.location = "login.html";
							},600);
						}
					)
					.error
					(
					function (ex) {
						Toast.fire({
							type: 'warning',
							title: ex.Message
						})
						setTimeout(function(){
							window.location = "login.html";
						},600);
					})
				}
			}

			$scope.activarUsuarioToken = function (token) { 
				ServiceRoutes.activarUsuarioToken({
					token: token
				}).success(
					function (response) {
						if (response.IsValid == true) {
							Toast.fire({
								type: 'success',
								title: 'Cuenta activada correctamente.'
							})
						}else {
							Toast.fire({
								type: 'warning',
								title: response.Message
							})
						}
						setTimeout(function(){
							window.location = "login.html";
						},500);
					}
				)
				.error
				(
				function (ex) {
					Toast.fire({
						type: 'warning',
						title: ex.Message
					})
					setTimeout(function(){
						window.location = "login.html";
					},500);
				})
			}
		}
	]);