angular.module('appRoot')
    .service
    (
    'ServiceRoutes',
    [
        '$http',
        'URLS_CONFIG',
        function ($http, URLS_CONFIG) {

            // la variables estan definidas en el archivo app.config.js
            var urlAportaws =  URLS_CONFIG.urlAportaws + "/api" ;
            var urlAportaPro = URLS_CONFIG.urlAportaPro + "/api";

            this.login = function (datos) {
                return $http.post(urlAportaws + '/Accessos/LoginEmpleados', datos);
            };
        }
    ]
);
