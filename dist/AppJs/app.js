// Main configuration file. Sets up AngularJS module and routes and any other config objects
var appRoot = angular.module('appRoot', [
    'ngRoute',
    'ngResource',
    'angular.filter',
    'LocalStorageModule',
    function () {
        
    }
]);     //Define the main module

appRoot.service('sharedProperties', function () {
    var isLogged = false;

    return {
        getIsLogged: function () {
            return isLogged;
        },
        setIsLogged: function (value) {
            isLogged = value;
        }
    };
});

appRoot.controller('RootController', ['$scope', 'sharedProperties', function ($scope, sharedProperties) {

    if (amplify.store('id_usuario')) {
       
    }

}]);

appRoot.config(['localStorageServiceProvider', function (localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('imv');
}]);

appRoot.config(['$routeProvider', function ($routeProvider) {

    var aux = window.location.href.split('#');
    var url = aux[0];
    if (url.indexOf('localhost') != -1) url = ''; // Si es localhost no moverle a las rutas de las páginas

    /* Fix para rutas en localhost, milan y productivo */
    var urlPref = "/";
    var host = window.location.hostname.split('.');
    var index = host.indexOf('milan');

    $routeProvider
        .when('/', { templateUrl: "../index.html", controller: 'indexController' })

    .otherwise({ redirectTo: "../index.html" });
}]);

appRoot.directive('currency', function () {
    return {
        require: 'ngModel',
        link: function(elem, $scope, attrs, ngModel){
            ngModel.$formatters.push(function(val){
                if (val == undefined){
                    return '$0.00'
                }
                return '$' + val
            });
            ngModel.$parsers.push(function(val){
                return val.replace(/^\$/, '')
            });
        }
    }
})

appRoot.directive('fcurrency', function ($filter) {
    return {
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel){
            ngModel.$formatters.push(function(val){
                if (!val){
                    return ''
                }
                return $filter('currency')(val);
            });
            ngModel.$parsers.push(function(val){
                return val.replace(/^\$/, '')
            });
            elem.bind('focus', function(){
                var valor = elem.val().replace(/[,$]/g, '');

                if(valor) {
                    elem.val(Number(valor));
                }
            });
            elem.bind('blur', function() {
                var valor = elem.val().replace(/[,$]/g, '');
                elem.val($filter('currency')(valor));
            });
        }
    }
})

appRoot.directive('caracteres', function () {
    return {
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel){
            elem.bind('keypress', function(event) {
                var patt = new RegExp(/[a-zA-Z\-À-ÿ\s.\u00F1\u00D1]/g);
                var caracter = event.key;

                if(!patt.test(caracter)) {
                    event.preventDefault();
                }
            });
        }
    }
})

appRoot.config(['$httpProvider', function ($httpProvider) {
    //Registrar el interceptor de peticiones
    $httpProvider.interceptors.push('httpInterceptor');
}]);
appRoot.filter('trusted', ['$sce', function ($sce) {
    return function (url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);
appRoot.factory('httpInterceptor', ['$q', '$injector', function ($q, $injector) {
    return {
        'request': function (config) {
            // request your $rootscope messaging should be here?
            mostrarCargando();
            return config;
        },

        'requestError': function (rejection) {
            // request error your $rootscope messagin should be here?
            removeloader();
            return $q.reject(rejection);
        },

        'response': function (response) {
            removeloader();

            return response;
        },

        'responseError': function (rejection) {
            // response error your $rootscope messagin should be here?
            removeloader();
            return $q.reject(rejection);

        }
    };
}]);

isMobile = function () {
    var isMobile = false;
    if( navigator.userAgent.match(/Android/i) 
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)){
            isMobile = true;
    } else {
        isMobile = false;
    }

    return isMobile;
};

mostrarCargando = function () {
    $("#loading-area").css("display", "block");
};

removeloader = function () {
    $("#loading-area").css("display", "none");
};
